function addTextById(id,text){
    document.getElementById(id).innerText = text;
}

var array = [];

function addArray() {
   
    var inputEl = document.getElementById("inputArray");
    if (inputEl.value ==""){
        return;
    }
    var number = inputEl.value*1;
    array.push(number)
    addTextById("txtArray",`${array}`)
    inputEl.value =""

    return array 
}

function quickSort(array){
    if (array.length < 2){
        return array;
    }

    var median = Math.floor(array.length/2);
    var pivot = array[median]
    const leftArr = [];
    const rightArr = [];

    for (step = 0; step < array.length; step++){
        if (step == median) continue;
        else if (array[step] < pivot){
            leftArr.push(array[step])
        }
        else {
            rightArr.push(array[step])
        }
    }

    return [...quickSort(leftArr),pivot,...quickSort(rightArr)]
}



function positiveSumCalculate() {
    for (var step = 0, sum = 0; step < array.length;step++){
        if (array[step] > 0){
            sum += array[step]
        }
    }
    addTextById("txtSum",`Tổng số dương ${sum}`)
}

function countPositiveNumber() {
    for (var step = 0, count = 0; step < array.length;step++){
        if (array[step] > 0){
            count += 1;
        }
    }
    addTextById("txtPositiveCount",`Số dương: ${count}`)
}

function findMin() {
    var sortedArray = quickSort(array);
    var min = sortedArray[0]
    addTextById("txtFindMin",`Số nhỏ nhất: ${min}`)
}

function findMinPositive() {
    var sortedArray = quickSort(array);
    for (var step = 0, min = 0; step < sortedArray.length; step++){
        if (sortedArray[step]*-1<0){
            min = sortedArray[step];
            addTextById("txtMinPositive",`Số dương nhỏ nhất: ${min}`)
            break;
        }
        else {
            addTextById("txtMinPositive","Không có số dương trong mảng")
        }
        
    }
    
}

function findLastEven(){
    
    for (var step = array.length - 1, maxEven = 0; step < array.length && step >= 0; step--) {
        if (array[step]%2==0){
            maxEven = array[step];
            addTextById("txtLastEven",`Số chẵn cuối cùng: ${maxEven}`)
            return
        }
    }
    if (maxEven==0){
        addTextById("txtLastEven",`-1`)
        return -1
    }
}

function changePosition() {
    var inputIndex1 = document.getElementById("inputIndex1").value*1;
    var inputIndex2 = document.getElementById("inputIndex2").value*1;
    var temp = array[inputIndex1];

    if (inputIndex1>array.length-1 || inputIndex2 >array.length-1){
        addTextById("txtChangePos",`Trong chuỗi chỉ có vị trí từ 0 tới ${array.length-1}.`)
    }
    else {
        array[inputIndex1]=array[inputIndex2];
        array[inputIndex2]=temp;
       
        addTextById("txtChangePos",`${array}`)
    }

    
    
}

function showSortedArr() {
    var sortedArray = quickSort(array);
    addTextById("txtSortUp",`${sortedArray}`)
}

function isPrimeNumber(inputNum) {
    
    for (var step = 2, count = 0; step <= inputNum; step++) {
        if (inputNum % step == 0) {
            count += 1;
        }
    }
    if (count >= 2) {
        return false
    }
    else if (count == 1) {
        return true
    }
}

function findFirstPrimeNum() {
    for ( step = 0, firstPrime = 0; step < array.length; step++){
        if (isPrimeNumber(array[step]) == true) {
            firstPrime = array[step]
            addTextById("txtFirstPrime",`Số nguyên tố đầu tiên: ${firstPrime}`)
            break
        }
    }
    if (firstPrime==0){
        addTextById("txtFirstPrime",`-1`)
        return -1
    }
   
}

var arrayEx9 = [];

function getFloat() {
    var addArray = document.getElementById("inputFloat").value*1;
    arrayEx9.push(addArray)
    addTextById("txtArrayFloat",`${arrayEx9}`)
   
    return arrayEx9
}

function findInt() {
    for (step = 0, count = 0; step<arrayEx9.length; step++){
        if (Number.isInteger(arrayEx9[step]) == true){
            count += 1;
        }
    }
    addTextById("txtInt",`Số nguyên: ${count}`)
}

function compareNum(){
    for (step = 0, countPositive = 0, countNegative = 0; step<array.length; step++){
        if (array[step]*-1>0){
            countNegative += 1;
        }
        else if (array[step]==0) continue;
        else if (array[step]*-1<0){
            countPositive +=1;
        }
    }
    if (countPositive>countNegative){
        addTextById("txtCompareNum",`Số dương > Số âm`)
    }
    else if (countPositive<countNegative){
        addTextById("txtCompareNum",`Số Âm > Số dương`)
    }
    else {
        addTextById("txtCompareNum",`Số Âm = Số dương`)
    }
}






